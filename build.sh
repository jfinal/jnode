#!/bin/sh
svn_path_ubuntu='/media/chunmenglu/LCM/svn/appid497670vuv6/'
svn_path_windows='/f/svn_git/appid497670vuv6/'

# 判断目录
if [ -d svn_path_ubuntu ]
then
    svn_path=$svn_path_ubuntu
else
    svn_path=$svn_path_windows
fi

# echo $svn_path
file_path='WebContent/'

# 获取参数 sh build.sh 1 后面的 1 为版本
if [ "$1" -gt 0 ] 2>/dev/null ;then
    args=$1
else
    args=0
fi

# 打包war
cd $file_path
jar cvf duapp.war *

# 移动war包到
target=$svn_path$args
[ -d target ] || mkdir $target

mv duapp.war $svn_path$args
# 执行svn操作提交war
cd $svn_path
svn update
svn ci -m "update"