package net.dreamlu.controller.api;

import java.util.Date;
import java.util.Map;

import net.dreamlu.api.oauth.OauthBaidu;
import net.dreamlu.config.Consts;
import net.dreamlu.model.User;
import net.dreamlu.model.WBLogin;

import org.apache.log4j.Logger;

import com.jfinal.core.Controller;

/**
 * 百度登录
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date Jun 27, 2013 12:40:11 AM
 */
public class BaiduLogin extends Controller {
    private static final Logger log = Logger.getLogger(QQLogin.class);
    private static final String PHOTO_URL = "http://tb.himg.baidu.com/sys/portrait/item/";
        
    public void index() {
        try {
            OauthBaidu baidu = new OauthBaidu();
            redirect(baidu.getAuthorizeUrl());
        } catch (Exception e) {
            log.error(e.getMessage());
            redirect("/");
        }
    }
    
    /**
     * 百度授权回调
     * @Title: callback
     * @param     设定文件
     * @return void    返回类型
     * @throws
     * 返回json：<url>http://developer.baidu.com/wiki/index.php?title=docs/oauth/rest/file_data_apis_list#.E8.8E.B7.E5.8F.96.E5.BD.93.E5.89.8D.E7.99.BB.E5.BD.95.E7.94.A8.E6.88.B7.E7.9A.84.E4.BF.A1.E6.81.AF</url>
     */
    public void callback() {
        try {
            OauthBaidu baidu = new OauthBaidu();
            // 获取用户登录信息
            Map<String, String> userInfo = baidu.getUserInfoByCode(getPara("code"));
            log.info(userInfo);
            String type = "baidu";
            String openid = userInfo.get("userid");
            String nickname = userInfo.get("username");
            String photoUrl = PHOTO_URL + userInfo.get("portrait");
            WBLogin login = WBLogin.dao.findByOpenID(openid, type);
            log.error("login:\t" + login);
            if(null == login) {
            	login = new WBLogin();
                login.set(WBLogin.OPENID, openid);
                login.set(WBLogin.TYPE, type);
                login.set(WBLogin.HEAD_PHOTO, photoUrl);
                login.set(WBLogin.CREATETIME, new Date());
                login.set(WBLogin.STATUS, WBLogin.STATUS_N);
                login.set(WBLogin.NICKNAME, nickname).save();
            }
            log.error("login:\t" + login.toJson());
            // 是否邮件校验通过
            int status = login.getInt(WBLogin.STATUS);
            if (null != login.getInt(WBLogin.ID) && WBLogin.STATUS_N == status) {
                if (null == login.getInt(WBLogin.USERID)) {
                    setAttr("nouser", true);
                }
                // 跳转到绑定页
                setAttr("id", login.getInt(WBLogin.ID));
                setAttr("type", type);
                setAttr("nickname", nickname);
                setAttr("photourl", photoUrl);
                render("binding");
                return;
            }
            if (WBLogin.STATUS_Y == status) {
            	User user = User.dao.findById(login.getInt(WBLogin.USERID));
                setSessionAttr(Consts.USER_SESSION, user);
            }
        }catch(Exception e){
            log.error(e);
        }
        redirect("/admin");
    }
}
