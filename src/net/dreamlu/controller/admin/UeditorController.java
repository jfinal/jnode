package net.dreamlu.controller.admin;

import java.util.List;

import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.utils.BCSUtils;

import org.apache.commons.lang.StringUtils;

import com.baidu.bae.api.util.BaeEnv;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StringKit;
import com.jfinal.log.Logger;
import com.jfinal.upload.UploadFile;

/**
 * 百度编辑器
 * @author L.cm
 * @date Nov 27, 2013 9:48:07 PM
 */
@Before(AdminInterceptor.class)
public class UeditorController extends Controller {

	private static final Logger logger = Logger.getLogger(UeditorController.class);

	// 上传图片
	public void uploadImg() {
		// 选择保存目录
		String fetch = getPara("fetch");
		if (StringKit.notBlank(fetch)) {
			renderJavascript("updateSavePath( [\"" + BCSUtils.bucket + "\"] );");
			return;
		}
		// 临时目录
		String tmpfsPath = BaeEnv.getTmpfsPath();
		try {
			UploadFile file = getFile("upfile", tmpfsPath);
			String fileName = file.getFileName();
			boolean isFail = BCSUtils.checkFileType(fileName, true);
			if (isFail) {
				setAttr("state", "图片类型不支持！");
			} else {
				// 上传文件
				logger.error("上传文件");
				setAttr("url", BCSUtils.uploadWatermark(file.getFile(), fileName));
				setAttr("state", "SUCCESS");
				
			}
			setAttr("original", fileName);
		} catch (Exception e) {
			logger.error(e.getMessage());
			setAttr("state", "图片上传失败，请稍后重试！");
		}
		// "SUCCESS", "SUCCESS"
		setAttr("title", getPara("pictitle"));
		renderJson(new String[]{"original", "url", "title", "state"});
		// "{'original':'"+up.getOriginalName()+"','url':'"+up.getUrl()+"','title':'"+up.getTitle()+"','state':'"+up.getState()+"'}"
	}

	// 上传文件
	public void uploadFile() {
		String tmpfsPath = BaeEnv.getTmpfsPath();
		UploadFile file = getFile("upfile", tmpfsPath);
		String fileName = file.getFileName();
		boolean isFail = BCSUtils.checkFileType(fileName, false);
		if (isFail) {
			setAttr("state", "文件类型不支持！");
		} else {
			try {
				setAttr("url", BCSUtils.upload(file.getFile(), fileName));
				setAttr("state", "SUCCESS");
			} catch (Exception e) {
				logger.error(e.getMessage());
				setAttr("state", "文件上传失败，请稍后重试！");
			}
		}
		setAttr("original", fileName);
		setAttr("fileType", BCSUtils.getFileType(fileName));
		renderJson(new String[]{"url", "fileType", "state", "original"});
		// {'url':'"+up.getUrl()+"','fileType':'"+up.getType()+"','state':'"+up.getState()+"','original':'"+up.getOriginalName()+"'}"
	}

	// 涂鸦上传
	public void uploadScrawl() throws Exception {
		String tmpfsPath = BaeEnv.getTmpfsPath();
		String newName = System.currentTimeMillis() + ".jpg";
		// jfinal必须先getfile,
		try {
			//String action = getPara("action");
			//if (StringKit.notBlank(action) && action.equals("tmpImg"))
			UploadFile file = getFile("upfile", tmpfsPath, 1024 * 1024);
			String url = BCSUtils.upload(file.getFile(), newName);
			//<script>parent.ue_callback('" + up.getUrl() + "','" + up.getState() + "')</script>
			renderText("<script>parent.ue_callback('" + url + "','SUCCESS')</script>");
		} catch (Exception e) {
			logger.error(e.getMessage());
			String content = getPara("content");
			setAttr("url", BCSUtils.uploadBase64(content, newName));
			setAttr("state", "SUCCESS");
			renderJson(new String[]{"url", "state"});
			// {'url':'" + up.getUrl()+"',state:'"+up.getState()+"'}
		}
	}

	// 图片管理
	public void imageManager() {
		List<String> list =BCSUtils.listObject(100);
		renderText(StringUtils.join(list, "ue_separate_ue"));
	}
}
