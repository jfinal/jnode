package net.dreamlu.model;


import com.jfinal.plugin.activerecord.Model;

/**
 * TODO 在此加入类描述
 * @author L.cm
 * @date 2013-06-23 23:39:58
 */
public class SpiderRule extends Model<SpiderRule> {
	
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "spider_rule";
	public static final String ID = "id";
	public static final String SITE_NAME = "siteName";
	public static final String DESCRIPTION = "description";
	public static final String SITE_URL = "siteUrl";
	public static final String LIST_RULE = "listRule";
	public static final String TITLE_RULE = "titleRule";
	public static final String CONTENT_RULE = "contentRule";
	public static final String ENABLED = "enabled";
	
	public static final SpiderRule dao = new SpiderRule();
}