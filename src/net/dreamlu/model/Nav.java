package net.dreamlu.model;

import java.util.List;

import net.dreamlu.config.Consts;

import com.jfinal.ext.plugin.db.DbModel;

/**
 * 导航
 * @author L.cm
 * @date Nov 13, 2013 9:54:05 PM
 */
public class Nav extends DbModel<Nav> {

    private static final long serialVersionUID = -3522681733218857968L;

    public static final Nav dao = new Nav();

    public static final String TABLE_NAME = "nav";
    public static final String ID         = "id";                // id
    public static final String PID        = "pid";               // pid 父id
    public static final String TITLE      = "title";             // title 标题，名称
    public static final String TARGET     = "target";            // target url
    public static final String ORDER_NUM  = "order_num";

    // 查询导航
	public List<Nav> findAll() {
		String sql = "SELECT n.* FROM nav n";
		return dao.findByCache(Consts.CACHE_TIME_MAX, sql);
	}
}
