package net.dreamlu.interceptor;

import java.util.List;

import net.dreamlu.model.Links;
import net.dreamlu.model.Options;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * 网站的基本设置
 * @author L.cm
 * @date 2013-5-30 下午9:44:56
 */
public class OptionsInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		ai.invoke();
		String controllerKey = ai.getControllerKey();
		// 网站的基本设置
        Options options = Options.dao.findByCache();
		if(!controllerKey.startsWith("/admin")){
	        // 网站的友情连接
	        List<Links> friendLinks = Links.dao.findListByType(0, Links.DEL_N);
	        ai.getController().setAttr("friendLinks", friendLinks);
		}
		ai.getController().setAttr("options", options);
	}

}
