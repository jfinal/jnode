package net.dreamlu.kit;

import java.util.List;
import java.util.Map;

import com.baidu.bae.api.factory.BaeFactory;
import com.baidu.bae.api.fetchurl.BaeFetchurl;
import com.baidu.bae.api.fetchurl.NameValuePair;
import com.jfinal.kit.StringKit;

/**
 * BAE 资源抓取
 * @author L.cm
 * @date Jun 15, 2013 7:48:18 PM
 */
public class FetchKit {
	
	private static final String UA = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36";
	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	
	/**
     * 发送get请求
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String doGet(String url) {
       return doGet(url, null, null, null);
    }
	
    /**
     * 发送get请求
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String doGet(String url, Map<String, String> header) {
       return doGet(url, null, header, null);
    }
    
    /**
     * 发送get请求
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String doGet(String url, Map<String, String> cookies, Map<String, String> header) {
       return doGet(url, cookies, header, null);
    }
    
	/**
	 * 发送get请求
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public static String doGet(String url, Map<String, String> cookies
	        , Map<String, String> header, String charSet) {
	    BaeFetchurl fetch= BaeFactory.getBaeFetchurl();
	    fetch.setAllowRedirect(true);
        fetch.setRedirectNum(2);
        fetch.setHeader("user-agent", UA);
        charSet = StringKit.isBlank(charSet) ? "UTF-8" : charSet;
        fetch.setHeader("Content-Type", "application/json; charset=" + charSet);
	    if(null != cookies) {
	        fetch.setCookies(cookies);
	    }
	    if (null != header) {
	        for (String key : header.keySet()) {
                fetch.setHeader(key, header.get(key));
            }
        }
	    fetch.get(url);
	    return fetch.getHttpCode() == 200 ? fetch.getResponseBody() : null;
	}
	
	/**
     * 发送post请求
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
	public static String doPost(String url, List<NameValuePair> data) {
	    return doPost(url, data, null, null, null);
    }
	
	/**
     * 发送post请求
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
	public static String doPost(String url, List<NameValuePair> data, Map<String, String> cookies
	        , Map<String, String> header, String charSet) {
        BaeFetchurl fetch= BaeFactory.getBaeFetchurl();
        fetch.setAllowRedirect(true);
        fetch.setRedirectNum(2);
        fetch.setHeader("user-agent", UA);
        charSet = StringKit.isBlank(charSet) ? "UTF-8" : charSet;
        fetch.setHeader("Content-Type", "text/html; charset=" + charSet);
        if(null != cookies) {
            fetch.setCookies(cookies);
        }
        if (null != data) {
            fetch.setPostData(data);
        }
        if (null != header) {
            for (String key : header.keySet()) {
                fetch.setHeader(key, header.get(key));
            }
        }
        fetch.post(url);
        return fetch.getHttpCode() == 200 ? fetch.getResponseBody() : null;
    }
	
	public static void main(String[] args) {
//	    List<NameValuePair> data = new ArrayList<NameValuePair>();
//	    BasicNameValuePair email = new BasicNameValuePair("email", "qq596392912@gmail.com");
//	    BasicNameValuePair pwd = new BasicNameValuePair("pwd", "*******");
//        data.add(email);
//        data.add(pwd);
//		System.out.println(doPost("http://www.dreamlu.net/admin/session", data));
		String url = "http://sandbox.api.simsimi.com/request.p?key=7ac29bcf-314a-43dd-9a18-e00d9c7ab96a&lc=ch&ft=1.0&text=";
		
		url = url + "我草";
		System.out.println(doGet(url));
	}
}