package net.dreamlu.kit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;

import org.apache.log4j.Logger;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

/**
 * YUICompressor压缩帮助类
 * @author L.cm
 * @date 2013-7-1 下午1:00:28
 */
public class YUICompressorKit {

    private static final Logger log = Logger.getLogger(YUICompressorKit.class);
    public static final String CHARSET = "UTF-8";
    
    public static void compressorHelper(String queryString, boolean isCss, Writer out) {
        List<String> fileList = StringsKit.comboList(queryString);
        Reader in = null;
        try {
            if (isCss) {
                for (String path : fileList) {
                    in = new InputStreamReader(new FileInputStream(path), CHARSET);
                    if(path.indexOf(".min.") > 0){
                        out.append(IOKit.readerToString(in));
                    }else{
                        CssCompressor css = new CssCompressor(in);
                        in.close(); in = null;
                        css.compress(out, -1);
                    }
                }
            }else{
                // nomunge: 混淆   verbose：显示信息消息和警告    preserveAllSemiColons：保留所有的分号
                // disableOptimizations 禁止优化
                boolean munge = true, verbose = false, preserveAllSemiColons = false, disableOptimizations = false;
                for (String path : fileList) {
                    in = new InputStreamReader(new FileInputStream(path), CHARSET);
                    if(path.indexOf(".min.") > 0){
                        out.append(IOKit.readerToString(in));
                    }else{
                        JavaScriptCompressor compressor = new JavaScriptCompressor(in, new ErrorReporter() {
                            public void warning(String message, String sourceName,
                                  int line, String lineSource, int lineOffset) {
                                if (line < 0) {
                                    log.error("\n[WARNING] " + message);
                                } else {
                                    log.error("\n[WARNING] " + line + ':' + lineOffset + ':' + message);
                                }
                            }
                            public void error(String message, String sourceName,
                                  int line, String lineSource, int lineOffset) {
                                if (line < 0) {
                                    log.error("\n[ERROR] " + message);
                                } else {
                                    log.error("\n[ERROR] " + line + ':' + lineOffset + ':' + message);
                                }
                            }
                            public EvaluatorException runtimeError(String message, String sourceName,
                                  int line, String lineSource, int lineOffset) {
                                error(message, sourceName, line, lineSource, lineOffset);
                                return new EvaluatorException(message);
                            }
                        });
                        in.close(); in = null;
                        compressor.compress(out, -1, munge, verbose, preserveAllSemiColons, disableOptimizations);
                    }
                }
            }
            out.flush();
        }catch(IOException e){
            log.error(e);
        }finally{
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }
    }
    public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {
        long aa = System.currentTimeMillis();
        Writer out = new OutputStreamWriter(new FileOutputStream("F:/aa.js"), CHARSET);
//      out = new OutputStreamWriter(System.out);
        // String queryString = "F:/css/reset.css,F:/css/style.css,F:/css/index.css,F:/css/css.css,F:/css/changejs.css,F:/css/aa.css&v=0.0.1";
        
        String queryString = "F:/css/jquery-1.8.3.js";
        boolean isCss = true;
        if (queryString.indexOf(".js") > 0) {
            isCss = false;
        }
        compressorHelper(queryString, isCss, out);
        long bb = System.currentTimeMillis();
        System.out.println(bb - aa);
    }
}
