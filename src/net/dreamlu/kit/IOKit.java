package net.dreamlu.kit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.log4j.Logger;

/**
 * 处理io流
 * @author L.cm
 * @date 2013-7-2 下午12:57:43
 */
public class IOKit {
	private static final Logger log = Logger.getLogger(IOKit.class);
    /**
     * 将reader转换成String
     * @param @param in
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String readerToString(Reader reader){
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder(); 
        try {
            br = new BufferedReader(reader); 
            String line; 
            while((line = br.readLine()) != null) { 
               sb.append(line); 
               sb.append("\r\n"); 
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    log.error(e);
                }
            }
        }
        return sb.toString();
    }
}
