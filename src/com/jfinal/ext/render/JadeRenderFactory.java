package com.jfinal.ext.render;

import com.jfinal.render.IMainRenderFactory;
import com.jfinal.render.Render;

/**
 * jade RenderFactory
 * @author L.cm
 * @date 2013-11-6 下午5:12:48
 */
public class JadeRenderFactory implements IMainRenderFactory {

    public static String viewExtension = ".jade";

    static {
        JadeRender.init();
    }
    
    public Render getRender(String view) {
        return new JadeRender(view);
    }

    public String getViewExtension() {
        return viewExtension;
    }
}
