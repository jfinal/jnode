# jnode
采用的jfinal java web框架 jade4j模板引擎 BAE云环境！

*删除了`BAE`已有的`jar`包，使用`BAE_eclise`可直接转成`BAE_java`项目。

`eclipse`用户可下载我整理出来的BAE环境`jar`，然后以用户`lib`导入。

[BAE_LIB_20130701.tar.gz](http://bcs.duapp.com/jfinaldemo/BAE_LIB_20130701.tar.gz)

### git and import to eclipse
```
 git clone http://git.oschina.net/596392912/jnode.git
```

# 安装
安装请查看[wiki](http://git.oschina.net/596392912/jnode/wikis/Home)

demo演示地址：
[http://www.dreamlu.net](http://www.dreamlu.net)

------
###```build.sh```为`eclipse`用户提供

1. 首先`svn checkout`出百度的项目
2. 更改`build.sh`中的```svn_path_ubuntu```和```svn_path_windows```
3. 使用`sh build.sh` 或 `sh build.sh 1`执行提交到百度svn ```1```为版本号 

注意`weindows`下本人是使用的[git base](https://code.google.com/p/msysgit/downloads/list)

如果有不明白或问题可联系email：596392912@qq.com Thanks！

## 鸣谢
1. [JFinal](http://www.oschina.net/p/jfinal)
2. [JFinal-ext](http://www.oschina.net/p/jfinal-ext)
3. [oschina](http://www.oschina.net/)
4. [portnine-free-bootstrap-theme](https://github.com/xiow/portnine-free-bootstrap-theme)
5. [wechat](http://git.oschina.net/gson/wechat)
6. [jade4j](https://github.com/neuland/jade4j)
7. [artDialog](http://www.planeart.cn/?page_id=660)
8. [bui](http://www.builive.com/start/index.php)


![dreamlu-weixin](http://www.dreamlu.net/images/weixin.jpg)

## License

( The MIT License )